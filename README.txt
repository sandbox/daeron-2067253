About
-----

The impala module is an attempt to provide a Git API for bare repositories. It
provides access right management and allows browsing through objects contained
by a repository.

Currently the impala_php_git module provides a native PHP implementation which
in time will be transitioned to a PHP extension which uses the libgit2 library.
The php-git extension was evaluated but lacks certain basic features and active
development.

Installation
------------

No installation instructions required as to only thing which needs to be done
is enabling the impala module.

Features/Improvements
---------------------

[impala]

 - Support submodules
 - Support revision walking
 - Support for creating branches and tags

[impala_php_git]

 - Allow raw objects to lazy load the object data so that large blob object
   load as fast as any other type of object, and only consume a minimum amount
   of memory. Currently this has not been implemented because there is a problem
   decompressing Git objects using zlib streams.
 - Transition to a php extension implementation which uses libgit2. Fork php-git
   and extends with new features which prevent impala from using it.

Module
------

Currently this module is only supported by Drupal 8 (8.x-1.x).
